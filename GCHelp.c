#include "GCHelp.h"

Graph* readGraph(char* FN){
    FILE *r = fopen(FN, "r");
    Graph *g = newGraph();
    if(r && !feof(r)){
        int nN;
        fscanf(r, "%d", &nN);
        int i;
        for(i = 0; i < nN && !feof(r); i++){
            int n;
            fscanf(r, "%d", &n);
            g = addNode(g, n);
        }
        while(!feof(r)){
            int a, b;
            fscanf(r, "%d %d", &a, &b);
            addEdge(g, a, b);
        }
    } else {
        printf("Nao foi possivel abrir o arquivo com o Grafo fonte!\r\nVerifique o arquivo e tente novamente.\r\n");
        exit(EXIT_FAILURE);
    }
    fclose(r);
    return g;
}

LinkedList* findConnectedComponents(Graph *g){
    if(!g){
        return NULL;
    } else {
        int n = sizeGraph(g);
        LinkedList *cc = newList();
        //Clone auxiliar do grafo
        Graph *c = cloneGraph(g);
        while(c){
            //Lista de nos conectados
            LinkedList *l = newList();
            //Fila de nos a vereficar
            Queue *q = newQueue();
            inQueue(q, findNode(g, c->id));
            l = addList(l, findNode(g, c->id));
            //Executa enquanto ha nos na fila de verificacao e a lista de nos conectados e menor que o numero de nos do grafo;
            while(!isEmptyQueue(q) && sizeList(l) != n){
                //Verifica as arestas do no da vez da fila
                Edge *e = ((Graph*) outQueue(q))->edges;
                while(e){
                    Graph *v = findNode(g, e->nB);
                    //Insere o no no qual o no em questao esta ligado caso ja nao esteja na lista
                    if(!findList(l, v, (void*) cmpNode)){
                        l = addList(l, v);
                        inQueue(q, v);
                    }
                    e = e->next;
                }
            }
            //Adiciona a componete conexa da vez na lista de reposta
            cc = addList(cc, l);
            //Remove do clone do grafo os nos que ja estao em uma componente conexa
            while(l){
                c = removeNode(c, ((Graph *) l->id)->id);
                l = l->next;
            }
            freeQueue(q);
        }
        return cc;
    }
}

LinkedList* findBridges(Graph *g){
    if(!g){
        return newList();
    } else {
        //Lista de pontes
        LinkedList *b = newList();
        //Clone auxiliar do grafo
        Graph *c = cloneGraph(g);
        Graph *n = c;
        LinkedList *pE = newList();
        while(n){
            Edge *e = n->edges;
            while(e){
                //Salvando as informacoes da aresta em questao
                int nA = e->nA;
                int nB = e->nB;
                e = e->next;
                Edge *p = findEdge(g, nA, nB);
                if(!findList(pE, p, (void*) cmpEdge)){
                    pE = addList(pE, p);
                    //Removendo a aresta em questao
                    removeEdge(c, nA, nB);
                    LinkedList *cc = findConnectedComponents(c);
                    //Verificando se a aresta era um ponte pelo numero de componetes conexas
                    if(cc && cc->next){
                        b = addList(b, p);
                    }
                    //Recolocando a aresta
                    addEdge(c, nA, nB);
                    if(cc){
                        LinkedList *h = cc;
                        while(h){
                            freeList(h->id);
                            h = h->next;
                        }
                    }
                    freeList(cc);
                }
            }
            n = n->next;
        }
        freeList(pE);
        return b;
    }
}

void writeGraph(char *FN, Graph *g){
    FILE *w = fopen(FN, "w");
    if(w){
        Graph *h = g;
        LinkedList *es = newList();
        fprintf(w, "%d\r\n", sizeGraph(g));
        while(h){
            fprintf(w, "%d\r\n", h->id);
            Edge *e = h->edges;
            while(e){
                if(!findList(es, e, (void*) cmpEdge)){
                    es = addList(es, e);
                }
                e = e->next;
            }
            h = h->next;
        }
        LinkedList *p = es;
        while(p){
            Edge *e = (Edge*) p->id;
            fprintf(w, "%d\t%d\r\n", e->nA, e->nB);
            p = p->next;
        }
        freeList(es);
    } else {
        printf("Nao foi possivel criar o arquivo com o Grafo atual!\r\nVerifique se ha espaco em disco e tente novamente.\n");
        exit(EXIT_FAILURE);
    }
    fclose(w);
}

void printGraph(Graph *g){
    Graph *h = g;
    LinkedList *es = newList();
    while(h){
        printf("%6d -> ", h->id);
        Edge *e = h->edges;
        while(e){
            printf("%d ", e->nB);
            e = e->next;
        }
        printf("\r\n");
        h = h->next;
    }
}

void writeConnectedComponents(char *FN, LinkedList *cc){
    FILE *w = fopen(FN, "w");
    if(w){
        fprintf(w, "Numero de compontes = %d\r\n", sizeList(cc));
        LinkedList *h = cc;
        int n = 1;
        while(h){
            fprintf(w, "%dª Componente\r\n\r\n", n++);
            LinkedList *c = (LinkedList*) h->id;
            LinkedList *es = newList();
            fprintf(w, "%d\r\n\r\n", sizeList(c));
            while(c){
                fprintf(w, "%d\r\n", ((Graph*) c->id)->id);
                Edge *e = ((Graph*) c->id)->edges;
                while(e){
                    if(!findList(es, e, (void*) cmpEdge)){
                        es = addList(es, e);
                    }
                    e = e->next;
                }
                c = c->next;
            }
            fprintf(w, "\r\n");
            while(es){
                Edge *e = (Edge*) es->id;
                fprintf(w, "%d\t%d\r\n", e->nA, e->nB);
                es = removeList(es, es->id, (void*) cmpEdge);
            }
            freeList(es);
            h = h->next;
        }
    } else {
        printf("Nao foi possivel criar o arquivo com as Componentes Conexas do Grafo atual!\r\nVerifique se ha espaco em disco e tente novamente.\n");
        exit(EXIT_FAILURE);
    }
    fclose(w);
}

void writeBridges(char *FN, LinkedList* b){
    FILE *w = fopen(FN, "w");
    if(w){
        if(b){
            fprintf(w, "Pontes:\r\n");
            while(b){
                fprintf(w, "%d\t%d\r\n", ((Edge*) b->id)->nA, ((Edge*) b->id)->nB);
                b = b->next;
            }
        } else {
            fprintf(w, "Nao ha pontes\r\n");
        }
    } else {
        printf("Nao foi possivel criar o arquivo com as Pontes do Grafo atual!\r\nVerifique se ha espaco em disco e tente novamente.\n");
        exit(EXIT_FAILURE);
    }
    fclose(w);
}