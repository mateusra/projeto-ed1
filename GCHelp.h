#include "FCmps.h"

/*Funcao de leitura de um grafo apartir de um arquivo de texto.*/
Graph* readGraph(char* FN);

/*Funcao de busca das componentes conexas de um grafo.*/
LinkedList* findConnectedComponents(Graph *g);

/*Funcao de busca das componentes conexas de um grafo.*/
LinkedList* findBridges(Graph *g);

/*Funcao de escrita de um grafo em um arquivo de texto.*/
void writeGraph(char *FN, Graph *g);

/*Funcao que imprime um grafo na tela.*/
void printGraph(Graph *g);

/*Funcao de escrita de uma lista de componetes conexas em um arquivo de texto.*/
void writeConnectedComponents(char *FN, LinkedList *cc);

/*Funcao de escrita de uma lista de pontes em um arquivo de texto.*/
void writeBridges(char *FN, LinkedList* b);