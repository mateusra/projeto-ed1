#include <stdio.h>
#include <stdlib.h>

/*Definacao da estrutura de aresta do grafo.*/
typedef struct Edge {
    int nA;
    int nB;
    struct Edge *next;
} Edge;

/*Definacao da estrutura de no do grafo.*/
typedef struct Graph {
    int id;
    struct Graph *next;
    Edge *edges;
} Graph;

/*Funcao que inicializa o grafo.*/
Graph* newGraph();

/*Funcao que adiciona um no ao grafo.*/
Graph* addNode(Graph *g, int n);

/*Funcao que adiciona uma aresta ao grafo.*/
void addEdge(Graph *g, int nA,int nB);

/*Funcao que remove um no do grafo.*/
Graph* removeNode(Graph *g, int n);

/*Funcao que remove uma aresta do grafo*/
void removeEdge(Graph *g, int nA, int nB);

/*Funcao que busca um no no grafo.*/
Graph* findNode(Graph *g, int n);

/*Funcao que busca um aresta no grafo.*/
Edge* findEdge(Graph *g,int nA,int nB); 

/*Funcao que cria um clone do grafo.*/
Graph* cloneGraph(Graph *g);

/*Funcao que calcula o numero de nos do grafo.*/
int sizeGraph(Graph *g);

/*Funcao que transforma uma lista de adjacencias em uma matriz de adjacencias*/
int** adjListToAdjTable(Graph *g);

/*Funcao que transforma uma matriz de adjacencias em um lista de adjacencias*/
Graph* adjTableToAdjList(int** t, int n);

/*Funcao que libera o grafo.*/
void freeGraph(Graph *g);