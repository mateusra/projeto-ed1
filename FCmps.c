#include "FCmps.h"

int cmpInt(int a, int b){
    return a == b;
}

int cmpDouble(double a, double b){
    return a == b;
}

int cmpChar(char a, char b){
    return a == b;
}

int cmpString(char * a, char *b){
    return !strcasecmp(a, b);
}

int cmpNode(Graph * a, Graph *b){
    if(!a && !b){
        return 1;
    } else if(!a || !b){
        return 0;
    } else {
        return a->id == b->id;
    }
}

int cmpEdge(Edge * a, Edge *b){
    if(!a && !b){
        return 1;
    } else if(!a || !b){
        return 0;
    } else {
        return((a->nA == b->nA) && (a->nB == b->nB)) || ((a->nA == b->nB) && (a->nB == b->nA));
    }
}

int cmpList(LinkedList *a, LinkedList *b, int (*cmp)(void*, void*)){
    while(a && b){
        if(cmp(a->id, b->id)){
            return 0;
        }
        a = a->next;
        b = b->next;
    }
    return 1;
}

int cmpQueue(Queue *a, Queue *b, int (*cmp)(void*, void*)){
    Queue *h1 = newQueue(), *h2 = newQueue();
    int equals = 1;
    while(isEmptyQueue(a) && isEmptyQueue(b)){
        void * x = outQueue(a), *y = outQueue(b);
        inQueue(h1, x);
        inQueue(h2, y);
        if(!cmp(x, y)){
            int equal = 0;
        }
    }
    if(!isEmptyQueue(a) || !isEmptyQueue(b)){
        equals = 0;
    }

    while(isEmptyQueue(h1)){
        inQueue(a, outQueue(h1));
    }
    while(isEmptyQueue(h2)){
        inQueue(b, outQueue(h2));
    }
    freeQueue(h1);
    freeQueue(h2);
    return equals;
}