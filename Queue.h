#include <stdio.h>
#include <stdlib.h>

/*Definacao da estrutura de elemento da fila.*/
typedef struct Element {
    void *id;
    struct Element *next;
} Element;

/*Definacao da estrutura de fila.*/
typedef struct Queue{
    Element *first, *last;
}Queue;

/*Funcao que verifica se a fila esta vazia.*/
int isEmptyQueue(Queue *q);

/*Funcao que colaca um elemento ao final da fila*/
void inQueue(Queue *q, void* elem);

/*Funcao que retira o elemento da vez da fila.*/
void* outQueue(Queue *q);

/*Funcao que inicializa a fila.*/
Queue* newQueue();

/*Funcao que libera a fila.*/
void freeQueue(Queue *q);