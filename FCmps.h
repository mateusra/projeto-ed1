#include <string.h>
#include "Queue.h"
#include "List.h"
#include "Graph.h"

/*Funcao de comparacao de inteiros.*/
int cmpInt(int a, int b);

/*Funcao de comparacao de reais.*/
int cmpDouble(double a, double b);

/*Funcao de comparacao de caracteres.*/
int cmpChar(char a, char b);

/*Funcao de comparacao de strings ignorando a capitulacao.*/
int cmpString(char * a, char *b);

/*Funcao de comparacao de nos de um grafo.*/
int cmpNode(Graph * a, Graph *b);

/*Funcao de comparacao de arestas deum grafo.*/
int cmpEdge(Edge * a, Edge *b);

/*Funcao de comparacao de listas.*/
int cmpList(LinkedList *a, LinkedList *b, int (*cmp)(void*, void*));

/*Funcao de comparacao de filas.*/
int cmpQueue(Queue *a, Queue *b, int (*cmp)(void*, void*));