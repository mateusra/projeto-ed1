#include "Graph.h"

Graph* newGraph(){
    return NULL;
}

Graph* addNode(Graph *g, int n){
    Graph *newN = (Graph*) malloc(sizeof(Graph));
    newN->id = n;
    newN->edges = NULL;
    newN->next = g;
    return newN;
}

void addEdge(Graph *g, int nA, int nB){
    Graph *n = findNode(g, nA);
    Edge *e = (Edge*) malloc(sizeof(Edge));
    e->nA = nA;
    e->nB = nB;
    e->next = n->edges;
    n->edges = e;

    n = findNode(g, nB);
    e = (Edge*) malloc(sizeof(Edge));
    e->nA = nB;
    e->nB = nA;
    e->next = n->edges;
    n->edges = e;
}

Graph* removeNode(Graph *g, int n){
    if(g){
        Graph *a = g, *p = NULL;
        while(a && a->id != n){
            p = a;
            a = a->next;
        }
        if(a){
            while(a->edges){
                removeEdge(g, a->edges->nA, a->edges->nB);
            }
            if(p){
                p->next = a->next;
            } else {
                g = g->next;
            }
            free(a);
        }
    }
    return g;
}

void removeEdge(Graph *g, int nA, int nB){
    Graph *n = findNode(g, nA);
    Edge *p = NULL, *a = n->edges;
    while(a->nB != nB){
        p = a;
        a = a->next;
    }
    if(p){
        p->next = a->next;
    } else {
        n->edges = a->next;
    }
    free(a);

    n = findNode(g, nB);
    p = NULL;
    a = n->edges;
    while(a->nB != nA){
        p = a;
        a = a->next;
    }
    if(p){
        p->next = a->next;
    } else {
        n->edges = a->next;
    }
    free(a);
}

Graph* findNode(Graph *g, int n){
    if(!g){
        return NULL;
    } else if(g->id == n){
        return g;
    } else {
        return findNode(g->next, n);
    }
}

Edge* findEdge(Graph *g, int nA, int nB){
    if(!g){
        return NULL;
    } else {
        Graph *n = findNode(g, nA);
        Edge *e = n->edges;
        while(e && e->nB != nB){
            e = e->next;
        }
        return e;
    }
}

Graph* cloneGraph(Graph *g){
    if(!g){
        return NULL;
    } else {
        Graph *cl = NULL, *h = g;
        while(h){
            cl = addNode(cl, h->id);
            h = h->next;
        }
        h = g;
        while(h){
            Edge *a = h->edges;
            while(a){
                if(!findEdge(cl, a->nA, a->nB)){
                    addEdge(cl, a->nA, a->nB);
                }
                a = a->next;
            }
            h = h->next;
        }
        return cl;
    }
}

int sizeGraph(Graph *g){
    if(g){
        return 1 + sizeGraph(g->next);
    } else {
        return 0;
    }
}

int** adjListToAdjTable(Graph *g){
    Graph *h = g;
    int n = sizeGraph(g) + 1;
    int** t = (int**) malloc(sizeof(int*)*n);
    int i;
    for(i = 0; i < n; i++){
        t[i] = (int*) malloc(sizeof(int)*n);
    }
    for(i = 0; i < n; i++){
        int j;
        for(j = 0; j < n; j++){
            t[i][j] = 0;
        }
    }
    for(i = 1; i < n; i++){
        t[0][i] = h->id;
        t[i][0] = h->id;
        h = h->next;
    }

    h = g;
    while(h){
        Edge *e = h->edges;
        while(e){
            int a = n - sizeGraph(h);
            int b = n - sizeGraph(findNode(g, e->nB));
            t[a][b] = 1;
            e = e->next;
        }
        h = h->next;
    }
    return t;
}

Graph* adjTableToAdjList(int** t, int n){
    Graph *h = newGraph();
    int i;
    for(i = 1; i < n; i++){
        h = addNode(h, t[0][i]);
    }

    for(i = 1; i < n; i++){
        int j;
        for(j = i; j < n; j++){
            if(t[i][j]){
                addEdge(h, t[0][i], t[j][0]);
            }
        }
    }
    return h;
}

void freeGraph(Graph *g){
    while(g){
        g = removeNode(g, g->id);
    }
}
