#include <stdio.h>
#include <stdlib.h>

/*Definacao da estrutura de lista simplesmente encadeada.*/
typedef struct LinkedList {
    void* id;
    struct LinkedList *next;
} LinkedList;

/*Funcao que inicializa a lista.*/
LinkedList* newList();

/*Funcao que adciona um elemento na lista.*/
LinkedList* addList(LinkedList *l, void* e);

/*Funcao que busca um elemento na lista.*/
LinkedList* findList(LinkedList *l, void *e, int (*cmp)(void*, void*));

/*Funcao que remove um elemento da lista.*/
LinkedList* removeList(LinkedList *l, void* e, int (*cmp)(void*, void*));

/*Funcao que calcula o tamanho da lista.*/
int sizeList(LinkedList *l);

/*Funcao que libera a lista.*/
void freeList(LinkedList *l);