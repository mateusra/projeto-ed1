#include "Main.h"
    
int main(int argc, char** argv){
    if(argc > 1){
        Graph *g = readGraph(argv[1]);
        help();
        char o[100];
        while(!cmpString(o, "-S")){
            scanf("%s%*c", o);
            if(cmpString(o, "-IN")){
                int n;
                while((scanf("%d%*c", &n)) == 1){
                    if(!findNode(g, n)){
                        g = addNode(g, n);
                    } else {
                        printf("Nao foi possivel a inserir, pois o no %d ja esta inserido no Grafo.\r\n", n);
                    }
                }
            } else if(cmpString(o, "-RN")){
                int n;
                while((scanf("%d%*c", &n)) == 1){
                    g = removeNode(g, n);
                }
            } else if(cmpString(o, "-IA")){
                int nA, nB;
                while((scanf("%d %d%*c", &nA, &nB)) == 2){
                    if(!findNode(g, nA) && !findNode(g, nB)){
                        printf("Nao foi possivel a inserir a aresta, pois tanto o no %d quanto o no %d nao estao inseridos no Grafo.\r\nDejesa inseri-los?[Y/N] ", nA, nB);
                        scanf("%s%*c", o);
                        if(cmpString(o, "Y")){
                            g = addNode(g, nA);
                            g = addNode(g, nB);
                        }
                    } else if(!findNode(g, nA)){
                        printf("Nao foi possivel a inserir a aresta, pois o no %d nao esta inserido no Grafo.\r\nDejesa inseri-lo?[Y/N] ", nA);
                        scanf("%s%*c", o);
                        if(cmpString(o, "Y")){
                            g = addNode(g, nA);
                        }
                    } else if(!findNode(g, nB)){
                        printf("Nao foi possivel a inserir a aresta, pois o no %d nao esta inserido no Grafo.\r\nDejesa inseri-lo?[Y/N] ", nB);
                        scanf("%s%*c", o);
                        if(cmpString(o, "Y")){
                            g = addNode(g, nB);
                        }
                    }
                    if(findNode(g, nA) && findNode(g, nB)){
                        addEdge(g, nA, nB);
                    }
                }
            } else if(cmpString(o, "-RA")){
                int nA, nB;
                while((scanf("%d %d%*c", &nA, &nB)) == 2){
                    if(findNode(g, nA) && findNode(g, nB) && findEdge(g, nA, nB)){
                        removeEdge(g, nA, nB);
                    }
                }
            } else if(cmpString(o, "-V")){
                printGraph(g);
            } else if(cmpString(o, "-CC")){
                DAS(argv[1], g);
            } else if(cmpString(o, "-A")){
                help();
            } else if(!cmpString(o, "-S")){
                printf("Opcao invalida, use -A para obter ajuda.\r\n");
            }
        }
        freeGraph(g);
        printf("Programa finalizado com sucesso!\r\n");
        return EXIT_SUCCESS;
    } else {
        printf("Faltam paramentros de inicializacao!\r\nInformar o nome do arquivo do grafo incial.\r\n");
        return EXIT_FAILURE;
    }
}

void DAS(char *OFN, Graph *g){
    char NFN[100];
    strcpy(NFN, "Novo - ");
    strcat(NFN, OFN);
    writeGraph(NFN, g);
    //Listando as Componetes Conexas
    LinkedList *cc = findConnectedComponents(g);
    if(cc && cc->next){
        printf("O grafo nao e conexo.\r\n");
        writeConnectedComponents("Componetes.txt", cc);
    } else {
        printf("O grafo e conexo.\r\n");
        //Listando as Pontes
        LinkedList *b = findBridges(g);
        writeBridges("Pontes.txt", b);
        freeList(b);
    }
    //Liberando as componentes conexas
    LinkedList *h = cc;
    while(h){
        freeList(h->id);
        h = h->next;
    }
    freeList(cc);
}

void help(){
    printf("Digite uma das opcoes abaixo no seguinte formato: -<OPCAO> <PARAM_1> <PARAM_2> ... <PARAM_N>:\r\n\r\n"
            "-IN <NO_1> <NO_2>...\t\t\t\tInsere uma lista de nos no Grafo;\r\n\r\n"
            "-RN <NO_1> <NO_2>...\t\t\t\tRetira uma lista de nos do Grafo;\r\n\r\n"
            "-IA <NO_A_1> <NO_B_1> <NO_A_2> <NO_B_2>...\tInsere uma lista de arestas, entre dois nos diferentes, no Grafo;\r\n\r\n"
            "-RA <NO_A_1> <NO_B_1> <NO_A_2> <NO_B_2>...\tRetira uma lista de arestas do Grafo;\r\n\r\n"
            "-V\t\t\t\t\t\tVisualiza o grafo atual na tela;\r\n\r\n"
            "-CC\t\t\t\t\t\tEscreve num arquivo de texto \"Novo - <NOME_DO_ARQUIVO_DE_ENTRADA>.txt\"\r\n\t\t\t\t\t\to Grafo atual, verifica se o grafo e conexo e, em caso afirmativo,\r\n\t\t\t\t\t\tescreve num arquivo \"Pontes.txt\" as pontes do grafo,"
            " senao escreve\r\n\t\t\t\t\t\tNum arquivo de texto \"Componentes.txt\" as Componentes Conexas do Grafo;\r\n\r\n"
            "-S\t\t\t\t\t\tSai do programa sem salvar nenhum arquivo;\r\n\r\n"
            "-A\t\t\t\t\t\tMostra essa ajuda.\r\n\r\n");
}