#include "List.h"

LinkedList* newList(){
    return NULL;
}

LinkedList* addList(LinkedList *l, void *e){
    LinkedList *n = (LinkedList*) malloc(sizeof(LinkedList));
    n->id = e;
    n->next = l;
    return n;
}

LinkedList* findList(LinkedList *l, void *e, int (*cmp)(void*, void*)){
    if(l && !cmp(l->id, e)){
        return findList(l->next, e, cmp);
    }
    return l;
}

LinkedList* removeList(LinkedList *l, void *e, int (*cmp)(void*, void*)){
    if(l && cmp(l->id, e)){
        LinkedList *h = l;
        l = l->next;
        free(h);
    } else if(l){
        l->next = removeList(l->next, e, cmp);
    }
    return l;
}

int sizeList(LinkedList * l){
    LinkedList *h = l;
    int n;
    for(n = 0; h; n++){
        h = h->next;
    }
    return n;
}

void freeList(LinkedList * l){
    if(l){
        freeList(l->next);
        free(l);
    }
}