#include "Queue.h"

Queue* newQueue(){
    Queue *q = (Queue*) malloc(sizeof(Queue));
    q->first = NULL;
    q->last = NULL;
    return q;
}

void inQueue(Queue *q, void *elem){
    Element *e = (Element*) malloc(sizeof(Element));
    e->id = elem;
    e->next = NULL;
    if(!q->first){
        q->first = e;
        q->last = e;
    } else {
        if(q->first == q->last){
            q->first->next = e;
            q->last = e;
        } else {
            q->last->next = e;
            q->last = e;
        }
    }
}

void* outQueue(Queue *q){
    Element *e = q->first;
    if(q->first == q->last){
        q->first = q->first->next;
        q->last = q->last->next;
    } else {
        q->first = q->first->next;
    }
    void *x = e->id;
    free(e);
    return x;
}

int isEmptyQueue(Queue *q){
    return q->first == NULL;
}

void freeQueue(Queue *q){
    while(!isEmptyQueue(q)){
        outQueue(q);
    }
    free(q);
}